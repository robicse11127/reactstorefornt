import React from 'react';
import ReactDom from 'react-dom';
import App from './App';

document.addEventListener( 'DOMContentLoaded', function() {
    var element = document.getElementById( 'react-store-front-theme' );
    if( typeof element !== 'undefined' && element !== null ) {
        ReactDom.render( <App />, document.getElementById( 'react-store-front-theme' ) );
    }
} )
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import FrontPage from './components/FrontPage';
import Products from './components/Product';
import SingleProduct from './components/SingleProduct';
import Single from './components/Single';
import Page from './components/Page';

import HeaderContextProvider from './contexts/HeaderContext';
import ProductContextProvider, { ProductContext } from './contexts/ProductsContext';
import PostsContextProvider from './contexts/PostsContext';

function App() {
    return(
        <Router>
            <HeaderContextProvider>
                <Header />
                    <Switch>
                        <Route exact path={'/'}>
                            <PostsContextProvider>
                                <FrontPage />
                            </PostsContextProvider>
                        </Route>
                        <Route exact path={ "/:slug" }>
                            <Single />
                        </Route>
                        <Route exact path={'/page/shop'}>
                            <ProductContextProvider>
                                <Products />
                            </ProductContextProvider>
                        </Route>
                        <Route exact path={ '/page/:slug' }>
                            <Page />
                        </Route>
                        <Route exact path={ '/product/:slug' }>
                            <SingleProduct />
                        </Route>

                    </Switch>
                <Footer />
            </HeaderContextProvider>
        </Router>
    )
}

export default App;
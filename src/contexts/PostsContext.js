import React, { useEffect, useState, createContext } from 'react';
import axios from 'axios';

export const PostsContext = createContext();

const PostsContextProvider = ( props ) => {
    const[ posts, setPosts ] = useState();
    const url = `${reactstore.apiUrl}/wp/v2/posts`;
    useEffect( () => {
        axios.get( url )
        .then( ( res ) => {
            setPosts( res.data )
        } )
        .catch( ( error ) => {
            console.log( 'Error: ' + error );
        } )
    },[] )

    return(
        <PostsContext.Provider value={ { posts, setPosts } }>
            { props.children }
        </PostsContext.Provider>
    )
}
export default PostsContextProvider;
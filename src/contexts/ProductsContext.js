import React, { useState, useEffect, createContext } from 'react';
import WooCommerceRestApi from '@woocommerce/woocommerce-rest-api';

export const ProductContext = createContext();

const ProductContextProvider = ( props ) => {
    const [ products, setProducts ] = useState();

    const Woo = new WooCommerceRestApi({
        url: `${reactstore.siteUrl}`,
        consumerKey: 'ck_664609fcbf54ccc377101e72537865c12918f67c',
        consumerSecret: 'cs_1f10afbe378a221a236b0ec9fe0410fa85b483e8',
        version: 'wc/v3',
    });

    /**
     * Fetch products.
     */
    useEffect( () => {
        Woo.get( 'products', {
            per_page: 20,
        } )
        .then( ( res ) => {
            if( undefined !== res ) {
                setProducts( res.data )
            }
        } )
        .catch( ( error ) => {
            console.log( error )
        } )
    }, [] );

    return (
        <ProductContext.Provider value={ { products } } >
            { props.children }
        </ProductContext.Provider>
    )
}

export default ProductContextProvider;
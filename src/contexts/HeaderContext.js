import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';

export const HeaderContext = createContext();

const HeaderContextProvider = ( props ) => {
    const [ blogInfo, setBlogInfo ] = useState();
    const [ menus, setMenus ] = useState();
    const blogInfoUrl = `${reactstore.apiUrl}/wp/v2/general`;
    const menusUrl = `${reactstore.apiUrl}/wp/v2/menus`;

    /**
     * Fetching Blog Info Data
     */
    useEffect( () => {
        axios.get( blogInfoUrl )
        .then( ( res ) => {
            setBlogInfo( res.data );
        } )
        .catch( ( error ) => {
            console.log( error )
        } )
    }, [blogInfo] );

    /**
     * Fetching Menus Data.
     */
    useEffect( () => {
        axios.get( menusUrl )
        .then( ( res ) => {
            setMenus( res.data );
        } )
        .catch( ( error ) => {
            console.log( error )
        } )
    }, [] );

    return (
        <HeaderContext.Provider value={ { blogInfo, menus} }>
            { props.children }
        </HeaderContext.Provider>
    )
}

export default HeaderContextProvider;
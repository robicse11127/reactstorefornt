import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { HeaderContext } from '../contexts/HeaderContext';

const Header = () => {
    /**
     * Destructure HeaderContext
     */
    const { blogInfo, menus } = useContext( HeaderContext );

    return (
        <React.Fragment>
            { undefined !== blogInfo && (
                <header>
                    <h2>{ blogInfo.site_title }</h2>
                    <p>{ blogInfo.site_tag_line }</p>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <div className="container">
                            <Link to={ '/' } className="navbar-brand">{ blogInfo.site_title }</Link>
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNav">
                                <ul className="navbar-nav">
                                    { undefined !== menus && menus['primary'].map( ( menu ) => {
                                        return(
                                            <li className="nav-item">
                                                { 'Page' === menu.parent.type_label ? (
                                                    <Link to={ `/page/${menu.parent.slug}` } className="nav-link">
                                                        { menu.parent.title }
                                                    </Link>
                                                ):(
                                                    <Link to={ `/${menu.parent.slug}` } className="nav-link">
                                                        { menu.parent.title }
                                                    </Link>
                                                )}
                                            </li>
                                        )
                                    } ) }
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>
            )}
        </React.Fragment>
    )
}

export default Header;

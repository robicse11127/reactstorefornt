import React, { useEffect, useState } from 'react';
import WooCommerceRestApi from '@woocommerce/woocommerce-rest-api';
import { useParams, Link } from 'react-router-dom';
import renderHTML from 'react-render-html';

const SingleProduct = () => {

    const [ product, setProduct ] = useState();
    const [ qty, setQty ] = useState( 1 );
    let { slug } = useParams();

    const Woo = new WooCommerceRestApi({
        url: `${reactstore.siteUrl}`,
        consumerKey: 'ck_664609fcbf54ccc377101e72537865c12918f67c',
        consumerSecret: 'cs_1f10afbe378a221a236b0ec9fe0410fa85b483e8',
        version: 'wc/v3',
    });

    useEffect( () => {

        Woo.get( 'products', {
            slug: slug
        } )
        .then( ( res ) => {
            setProduct( res.data )
            console.log( res.data )
        } )
        .catch( ( error ) => {
            console.log( error )
        } )

    }, [] );

    return(
        <React.Fragment>
            <div className="container">
                    { undefined !== product && (
                        <div className="row">
                            <div className="col-sm-6">
                                { undefined !== product[0].images && product[0].images.map( ( img ) => {
                                    return(
                                        <img className="image-fluid" src={ img.src } alt={ img.name } />
                                    )
                                } ) }
                            </div>
                            <div className="col-sm-6">
                                <h4 className="single-product-title">{ product[0].name }</h4>
                                <div className="price-tag">
                                    { renderHTML( product[0].price_html ) }
                                </div>
                                <div className="short-desc">
                                    { renderHTML( product[0].short_description ) }
                                </div>
                                <div className="add-to-cart">
                                    <input type="number" name="qty" min="1" step="1" value={ qty } onChange={ (e) => setQty( e.target.value ) }/>
                                    <button className="btn btn-primary add-to-cart">Add to cart</button>
                                </div>
                                <div className="product-meta">
                                    <p className="product-sku"><strong>SKU: </strong>{ product[0].sku }</p>
                                    <p className="product-stock"><strong>Stock: </strong>{ product[0].stock_status }</p>
                                    <p className="product-cats">
                                        <strong>Category: </strong>
                                        { product[0].categories.length > 0 && product[0].categories.map( ( term ) => {
                                            return(
                                                <Link className="term" to={ `/${term.slug}` }>{ term.name } </Link>
                                            )
                                        } ) }
                                    </p>
                                    <p className="product-tags">
                                        <strong>Tags: </strong>
                                        { product[0].tags.length > 0 && product[0].tags.map( ( tag ) => {
                                            <Link className="product-tags" to={ `/${tag.slug}` }>{ tag.name }</Link>
                                        } ) }
                                    </p>
                                </div>
                            </div>
                        </div>
                    ) }
            </div>
        </React.Fragment>
    )

}
export default SingleProduct;
import React, { useState, useEffect, useContext } from 'react';
import renderHTML from 'react-render-html';
import { Link } from 'react-router-dom';
import { ProductContext } from '../contexts/ProductsContext';


const Products = () => {
    const { products } = useContext( ProductContext );
    console.log( products );

    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    { undefined !== products && products.map( ( product ) => {
                        return(
                            <div className="col-md-3 card mb-3 product-item">
                                <img className="card-img-top" src={ product.images[0].src } alt={ product.images.name } />
                                <div className="card-body">
                                    <Link to={`/product/${product.slug}`}>
                                        <h4>{ product.name }</h4>
                                    </Link>
                                    <div className="price-tags">{ renderHTML( product.price_html ) }</div>
                                    <a rel="nofollow" data-quantity="1" data-product_id={ product.id } data-product_sku={ product.sku } className="btn btn-primary" href="">Add to cart</a>
                                </div>
                            </div>
                        )
                    } ) }
                </div>
            </div>
        </React.Fragment>
    )
}

export default Products;
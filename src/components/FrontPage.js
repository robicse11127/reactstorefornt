import React from 'react';
import Posts from './Posts';

const Frontpage = () => {
    return(
        <div>
            <Posts />
        </div>
    )
}

export default Frontpage;
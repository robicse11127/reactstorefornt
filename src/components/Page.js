import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import renderHTML from 'react-render-html';

const Single = () => {

    let { slug } = useParams();
    const [ page, setPage ] = useState();
    const [ notFound, setNotFound ] = useState(false);
    const url = `${reactstore.apiUrl}/wp/v2/pages`;
    useEffect( () => {
        axios.get( url, {
            params: {
                slug: slug
            }
        } )
        .then( ( res ) => {
            if( res.data.length <= 0 ) {
                setNotFound( true );
            } else {
                setPage( res.data );
            }
        } )
    },[ slug ] );

    return (
        <div className="container">
            { true === notFound ? (
                <h2>Not Found</h2>
            ) : (
                <div className="row">
                <div className="col-md-12">
                    { undefined !== page &&
                        <div className="card single-page">
                            {/* { '' !== page[0].featured_image_src && (
                                <img className="image-fluid" src={ page[0].featured_image_src.full } alt={ page[0].title.slug } />
                            ) } */}
                            <div className="card-body">
                                <h1>{ page[0].title.rendered }</h1>
                                <div className="">
                                    { renderHTML( page[0].content.rendered ) }
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </div>
            ) }

        </div>
    )


}
export default Single;
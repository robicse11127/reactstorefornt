import React, { useState, useEffect } from 'react';
import renderHTML from 'react-render-html';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';

const Single = () => {

    let { slug } = useParams();
    const [ post, setPost ] = useState();
    const [ notFound, setNotFound ] = useState(false);
    const url = `${reactstore.apiUrl}/wp/v2/posts`;
    useEffect( () => {
        axios.get( url, {
            params: {
                slug: slug
            }
        } )
        .then( ( res ) => {
            if( res.data.length <= 0 ) {
                setNotFound( true );
            } else {
                setPost( res.data );
            }
        } )
    },[ slug ] );

    return (
        <div className="container">
            { true === notFound ? (
                <h2>Not Found</h2>
            ) : (
                <div className="row">
                <div className="col-md-9">
                    { undefined !== post &&
                        <div className="card single-post">
                            <img className="image-fluid" src={ post[0].featured_image_src.full } alt={ post[0].title.slug } />
                            <div className="card-body">
                                <h1>{ post[0].title.rendered }</h1>
                                <div className="post-meta">
                                    <span className="post-author">
                                        <strong>Posted By:
                                            <Link to={ `/` }>{ post[0].author_details.user_nicename }</Link>
                                        </strong>
                                    </span>
                                    <span className="post-author">
                                        <strong>Published On:
                                            { post[0].published_on }
                                        </strong>
                                    </span>
                                    <span className="post-terms">
                                        <strong> In:
                                        {
                                            post[0].post_terms.map((term) => {
                                                return(
                                                    <Link to={'/category/'+term.id+'/posts'} key={term.id}>{term.name}, </Link>
                                                )
                                            })
                                        }
                                        </strong>
                                    </span>
                                </div>
                                <div className="post-content">
                                    { renderHTML( post[0].content.rendered ) }
                                </div>
                            </div>
                        </div>
                    }
                </div>
                <div className="col-md-3">
                    Sidebar
                </div>
            </div>
            ) }

        </div>
    )


}
export default Single;
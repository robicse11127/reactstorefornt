import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { PostsContext } from '../contexts/PostsContext';

const Posts = () => {

    const { posts } = useContext( PostsContext );

    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    { undefined !== posts && posts.map( ( post ) => {
                        return(
                            <div className="col-md-4 car post-item">
                                <img src={ post.featured_image_src.large } className="card-img-top" alt={ post.slug } />
                                <div className="card-body">
                                    <Link to={ `/${post.slug}` } className="post-title">
                                        <h2>{ post.title.rendered }</h2>
                                    </Link>
                                </div>
                            </div>
                        )
                    } ) }
                </div>
            </div>

        </React.Fragment>
    );

}
export default Posts;
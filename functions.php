<?php
/**
 * This file contains all core functionalities of the theme.
 */

if( ! defined( 'ABSPATH' ) ) : exit(); endif; // No direct access allowed.

/**
 * Define theme constants.
 */
define( 'REACTSTORE_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'REACTSTORE_URL', trailingslashit( plugins_url( '/', __FILE__ ) ) );

/**
 * Loading necessary scripts.
 */
function load_scripts() {
    wp_enqueue_script( 'reactstore-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', [ 'jquery' ], wp_rand(), true );
    wp_enqueue_script( 'reactstore-public', get_template_directory_uri() . '/dist/bundle.js', [ 'jquery', 'wp-element' ], wp_rand(), true );
    wp_localize_script( 'reactstore-public', 'reactstore', [
        'siteUrl' => home_url(),
        'apiUrl' => home_url( '/wp-json' ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );

    wp_enqueue_style( 'reactstore-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', [], wp_rand(), 'all' );
}
add_action( 'wp_enqueue_scripts', 'load_scripts' );

if ( ! function_exists( 'reactstorefront_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function reactstorefront_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on reactstorefront, use a find and replace
		 * to change 'reactstorefront' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'reactstorefront', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' 	=> esc_html__( 'Primary', 'reactstorefront' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'reactstorefront_theme_setup' );